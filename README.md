# Angular navigation bar assignment

* Made by: Mahesh Mehandiratta (Employee code: 3146489)
* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.
* Check the live hosted Navigation bar here: http://translator-beaver-42826.bitballoon.com/ 
* This is the second assignment given under Angular assignments
* Total of 5 components have been used to make the navbar

## How do I get set up? 

* Download zip of code and extract it
* Open project in your IDE
* Go to directory of the extracted code inside terminal
* Run "npm install"
* Run "ng serve"
* Open "http://localhost:4200/"

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
